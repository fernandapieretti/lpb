import axios from 'axios';
import { toast } from 'react-toastify';

// your api base url
const api = axios.create({
    baseURL: 'http://localhost:8000/api'
});

// add bearer token to all requests
api.interceptors.request.use((config) => {
    const token = localStorage.getItem('token');
    if (token) {
        config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
});

// intercept error
api.interceptors.response.use((response) => response, (error) => {
    if (!error.response) {
        toast.error('Houve um erro na conexao.');
    } else if (error.response) {
        toast.error(error.response.data.message);
    } else if (error.request) {
        toast.error(error.request);
    } else {
        toast.error(error.message);
    }
    throw error;
});

export default api;