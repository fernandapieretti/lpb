import React, { useState } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import { Layout, Login } from './Componentes/index';
import 'react-toastify/dist/ReactToastify.css';
import './App.css';

const App = () => {
  const [isAuthenticated, setAuthenticated] = useState<string | null>(localStorage.getItem('token'));

  return (
    <React.Fragment>
      <Router>
        <Switch>

          <Route path="/" exact render={() => !isAuthenticated
            ? <Login setAuthenticated={setAuthenticated} />
            : <Redirect to='/admin' />
          }></Route>

          <Route path="/admin" render={() => isAuthenticated
            ? <Layout setAuthenticated={setAuthenticated} />
            : <Redirect to='/' />
          }></Route>

        </Switch>
      </Router>

      <ToastContainer
        position="bottom-right"
        autoClose={3000}
        hideProgressBar={true}
        newestOnTop={true}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable={false}
        pauseOnHover={true} />
    </React.Fragment>
  );
}

export default App;
