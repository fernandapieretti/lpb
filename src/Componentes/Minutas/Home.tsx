import React from 'react';
import { AdminHome } from '../index';
import { PropsListingModel } from '../Shared/Admin/Listing/Model';
import { TableHeaderModel } from '../Shared/Table/Model';

const Home = ({ term }: PropsListingModel) => {
    const title: string = 'Minutas';
    
    const directory: string = 'minutas';
    
    const header: TableHeaderModel[] = [
        { headerName: 'Minutas', field: 'minuta' },
        { headerName: 'Ações', field: 'acoes' }
    ];

    return (
        <AdminHome title={title} directory={directory} header={header} term={term} />
    );
};

export default Home;