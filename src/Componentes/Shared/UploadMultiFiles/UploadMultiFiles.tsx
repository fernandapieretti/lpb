import React from 'react';
import { OnChangeEventModel } from '../Models/Generic';

const UploadMultiFiles = ({ field, onSelectDocuments, isLoading, acceptedExtensions }: any) => {

    const acceptedExtensionsRegex = new RegExp(acceptedExtensions);

    const hasUnacceptedExtension = (documents: FileList): boolean => {
        const names = Array.prototype.map.call(documents, ((doc: File) => doc.name));
        return names.some((document: any) => !acceptedExtensionsRegex.test(document.split('.')[1]));
    };

    const handleChange = (event: any) => {
        const documents = event.target.files;

        if (hasUnacceptedExtension(documents)) {
            console.log('hasUnacceptedExtension');
            return;
        }

        const selectedDocuments: OnChangeEventModel = {
            target: { name: field, value: documents }
        };

        onSelectDocuments(selectedDocuments);
    };

    return (
        <input type="file" className="form-control" name="documents" onChange={handleChange} disabled={isLoading} multiple />
    );
};

export default UploadMultiFiles;