export type TableHeaderModel = {
    field: string,
    headerName: string
};

export type TableContentModel = {
    id: number
};


export type PropsType = {
    header: TableHeaderModel[],
    data: any,
    directory: string, 
    handleDelete(id: number): Promise<void> | void,
    handleChangePage(page: number): Promise<void> | void,
    isLoading: boolean
};