import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { LoadingBar, Pagination } from '../../index';
import { TableHeaderModel, TableContentModel, PropsType } from './Model';

const Table = ({ header, data, directory, handleDelete, handleChangePage, isLoading }: PropsType) => {

    const qtdPages: number = 10;
    const [pages, setPages] = useState<number[]>([]);

    const renderHeader = header.map((item: TableHeaderModel) => {
        return (<th className="td-actions" key={item.field}>{item.headerName}</th>)
    });

    const renderContent = data && data.data.map((item: any) => {
        return (
            <tr key={item.id}>
                {header.map((key: TableHeaderModel) => {
                    if (key.field === 'acoes') {
                        return (
                            <td key={key.field + item.id}>
                                <Link to={'/admin/' + directory + '/editar/' + item.id}>
                                    <button className="btn mr-3">
                                        <i className="fas fa-pen"></i>
                                    </button>
                                </Link>
                                <button className="btn" onClick={() => handleDelete(item.id)}>
                                    <i className="fas fa-trash-alt"></i>
                                </button>
                            </td>
                        );
                    }
                    return (
                        <td key={key.field + item.id}>{item[key.field]}</td>
                    );
                })}
            </tr>
        );
    });

    const renderEmpty = () => {
        if (!isLoading && data && data.data.length === 0) {
            return (
                <tr>
                    <td className="text-center" colSpan={header.length}>Nenhum registro cadastrado.</td>
                </tr>
            );
        }
    };

    useEffect(() => {
        if (data) {
            const length = data.total > (qtdPages * data.per_page) ? qtdPages : Math.ceil(data.total / data.per_page);
            let inicio = data.current_page - (qtdPages / 2);
            if (data.last_page - data.current_page < (qtdPages / 2)) {
                inicio = data.last_page - qtdPages + 1;
            }
            inicio = inicio < 1 ? 1 : inicio;
            setPages(Array.from({ length: length }, (v, k) => k + inicio));
        }
    }, [data]);

    return (
        <React.Fragment>
            <div className="card">
                <div className="table-responsive">

                    <LoadingBar isLoading={isLoading} />

                    <table className={isLoading ? 'table table-hover table-striped loading' : 'table table-hover table-striped'}>

                        <thead>
                            <tr>{renderHeader}</tr>
                        </thead>

                        <tbody>
                            {renderEmpty()}
                            {renderContent}
                        </tbody>

                    </table>
                </div>
            </div>

            <Pagination
                pages={pages}
                qtdPages={qtdPages}
                currentPage={data?.current_page}
                lastPage={data?.last_page}
                handleChangePage={handleChangePage}
            />
        </React.Fragment>
    );
};

export default Table;