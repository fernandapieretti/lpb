import { TableHeaderModel } from "../../Table/Model";

export type PropsType = {
    title: string,
    header: TableHeaderModel[],
    directory: string,
    term: string
};