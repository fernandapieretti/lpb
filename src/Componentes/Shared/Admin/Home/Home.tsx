import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { Button, AdminLayout, AdminListing, AdminManage } from '../../../index';
import { PropsType } from './Model';

const Home = ({ title, header, directory, term }: PropsType) => {

    const botaoCadastrar = () => {
        const iconeCadastrar: string = 'fa-plus';
        const lblCadastrar: string = 'Cadastrar';
        const urlCadastrar: string = `/admin/${directory}/cadastrar`;
        return (
            <Button url={urlCadastrar} icone={iconeCadastrar} label={lblCadastrar} />
        )
    };
    
    const botaoCancelar = () => {
        const iconeCancelar: string = 'fa-arrow-left';
        const lblCancelar: string = 'Voltar';
        const urlCancelar: string = `/admin/${directory}`;
        return (
            <Button url={urlCancelar} icone={iconeCancelar} label={lblCancelar} />
        )
    };

    return (
        <Switch>
            <Route exact path={`/admin/${directory}`}>
                <AdminLayout button={botaoCadastrar()} title={title}>
                    <AdminListing header={header} directory={directory} term={term} />
                </AdminLayout>
            </Route>
            <Route path={[`/admin/${directory}/cadastrar`,`/admin/${directory}/editar/:id`]}>
                <AdminLayout button={botaoCancelar()} title={title}>
                    <AdminManage directory={directory} button={botaoCancelar()} />
                </AdminLayout>
            </Route>
        </Switch>
    );
};

export default Home;