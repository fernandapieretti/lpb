import React from 'react';
import { PropsType } from './Model';

const Layout = ({ title, button, children }: PropsType) => {
    return (
        <div>
            <div className="header">
                <h4 className="title">{ title }</h4>
                { button }
            </div>
            { children }
        </div>
    );
};

export default Layout;