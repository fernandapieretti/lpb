import React, { useEffect, useState } from 'react';
import api from '../../../../api/Connect';
import { confirmAlert } from 'react-confirm-alert';
import { toast } from 'react-toastify';
import { Table } from '../../../index';
import { ResultDataModel } from '../../../Shared/Models/Generic';

const Manage = ({ header, directory, term }: any) => {
    const [data, setData] = useState<ResultDataModel | null>();
    const [isLoading, setLoading] = useState<boolean>(false);
    const [page, setPage] = useState<number>(1);

    const initAction = () => {
        setData(null);
        setLoading(true);
    };

    const finishAction = () => {
        setPage(1);
        setLoading(false);
    };

    const handleChangePage = (page: number) => {
        setPage(page);
    };

    const handleDelete = (id: number) => {
        confirmAlert({
            title: 'Deseja excluir?',
            buttons: [
              { label: 'SIM', onClick: () => deleteItem(id) },
              { label: 'CANCELAR', onClick: () => null }
            ]
        });
    };

    const deleteItem = (id: number) => {
        const deleteItemAction = async () => {
            try {
                await api.delete(`/${directory}/${id}`);
                toast.success('Deleted');
                finishAction();
            } catch (error) {
                finishAction();
            };
        };
        initAction();
        deleteItemAction();
    };

    useEffect(() => {
        const getData = async () => {
            try {
                const request = term ? `/${directory}/page/${page}/${term}` : `/${directory}/page/${page}`;
                const response = await api.get(request);
                setData(response.data);
                setLoading(false);
            } catch (error) {
                setLoading(false);
            };
        };
        setLoading(true);
        getData();
    }, [page, term]);

    useEffect(() => {
        setPage(1);
    }, [term]);

    return (
        <div>
            <Table 
                header={header} 
                data={data} 
                directory={directory}
                isLoading={isLoading} 
                handleDelete={handleDelete}
                handleChangePage={handleChangePage} />
        </div>
    );
};

export default Manage;