import { FormEvent } from "react";

export type PropsManageModel<T> = {
    handleSubmit(e: Event|FormEvent<HTMLFormElement>, data: T|any): Promise<void>|void;
    data: T|any,
    isLoading: boolean,
    button: any
};