import React, { useState, useEffect } from 'react';
import api from '../../../../api/Connect';
import { useParams, useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import { LoadingBar, ManageMinutas, ManageAcoes, ManageClientes, ManageMeusDados, ManageModelosAcao, ManageUsuarios } from '../../../index';

const Manage = ({ directory, button }: any) => {
    const history = useHistory();
    const params = useParams<any>();
    const id = params.id;

    const [isLoading, setLoading] = useState(false);
    const [data, setData] = useState({});

    const handleSubmit = (event: Event, data: any) => {
        event.preventDefault();
        id ? update(data) : create(data);
    };

    const create = async (data: any) => {
        setLoading(true);
        try {
            const response = await api.post(`/${directory}`, data);
            setData(response.data);
            setLoading(false);
            toast.success('Created');
            history.push(`/admin/${directory}/editar/${response.data.id}`);
        } catch (error) {
            setLoading(false);
        };
    };

    const update = async (data: any) => {
        setLoading(true);
        try {
            const response = await api.put(`/${directory}`, data);
            setData(response.data);
            setLoading(false);
            toast.success('Updated');
        } catch (error) {
            setLoading(false);
        };
    };

    const renderComponentByDirectory = () => {
        switch (directory) {
            case 'acoes':
                return <ManageAcoes handleSubmit={handleSubmit} data={data} isLoading={isLoading} button={button} />;
            case 'clientes':
                return <ManageClientes handleSubmit={handleSubmit} data={data} isLoading={isLoading} button={button} />;
            case 'meus-dados':
                return <ManageMeusDados handleSubmit={handleSubmit} data={data} isLoading={isLoading} button={button} />;
            case 'minutas':
                return <ManageMinutas handleSubmit={handleSubmit} data={data} isLoading={isLoading} button={button} />;
            case 'modelos-de-acao':
                return <ManageModelosAcao handleSubmit={handleSubmit} data={data} isLoading={isLoading} button={button} />;
            case 'usuarios':
                return <ManageUsuarios handleSubmit={handleSubmit} data={data} isLoading={isLoading} button={button} />;
        };
    };

    useEffect(() => {
        if (params.id) {
            const getItem = async () => {
                try {
                    const response = await api.get(`/${directory}/${params.id}`);
                    setData(response.data);
                    setLoading(false);
                } catch (error) {
                    setLoading(false);
                };
            };
            setLoading(true);
            getItem();
        }
    }, []);

    return (
        <React.Fragment>

            <LoadingBar isLoading={isLoading} />

            {renderComponentByDirectory()}

        </React.Fragment>
    );
};

export default Manage;