import React from 'react';

const Pagination = ({ pages, qtdPages, currentPage, lastPage, handleChangePage }: any) => {

    const prevNav = () => {
        if (pages[0] > 1) {
            return (
                <li className="page-item">
                    <a href="#" className="page-link default">...</a>
                </li>
            );
        }
    };

    const nextNav = () => {
        if (pages[qtdPages - 1] < lastPage) {
            return (
                <li className="page-item">
                    <a href="#" className="page-link default">...</a>
                </li>
            );
        }
    };

    const renderPages = pages.map((item: number) => {
        return (
            <li key={item} className={`${ currentPage === item ? 'active ' : ''} + page-item`}>
                <a href="#" onClick={() => handleChangePage(item)} className="page-link">{ item }</a>
            </li>
        );
    });

    return (
        <nav aria-label="Navegação">
            <ul className="pagination justify-content-center flex-wrap">
                <li className="page-item">
                    <a href="#" onClick={() => handleChangePage(1)} className="page-link">Primeira</a>
                </li>

                { prevNav() }

                { renderPages }
                
                { nextNav() }
                
                <li className="page-item">
                    <a href="#" onClick={() => handleChangePage(lastPage)} className="page-link">Última</a>
                </li>
            </ul>
        </nav>
    );
};

export default Pagination;