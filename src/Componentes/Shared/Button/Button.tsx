import React from 'react';
import { Link } from 'react-router-dom';

const Button = ({ url, icone, label }: any) => {
    return (
        <Link to={url}>
            <span className="btn"><i className={`fas ${icone}`}></i> {label}</span>
        </Link>
    );
};

export default Button;