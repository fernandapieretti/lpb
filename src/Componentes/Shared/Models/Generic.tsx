export type OnChangeEventModel = {
    target: {
        name: string,
        value: number | string
    }
};

export type NavLinksModel = {
    link: string,
    icone: string,
    name: string
};

export type ResultDataModel = {
    current_page: number,
    data: any[],
    last_page: number,
    per_page: number,
    total: number
};