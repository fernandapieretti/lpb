import React from 'react';

const LoadingBar = ({ isLoading }: any) => {
    if (isLoading) {
        return (
            <div className="load-bar">
                <div className="bar"></div>
                <div className="bar"></div>
                <div className="bar"></div>
            </div>
        );
    }
    return null;
};

export default LoadingBar;