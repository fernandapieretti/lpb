export type LoginModel = {
    user: string,
    password: string
};

export type PropsType = {
    setAuthenticated(token: string): any;
};