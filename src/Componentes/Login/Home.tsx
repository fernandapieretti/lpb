import React, { useState, FormEvent } from 'react';
import { OnChangeEventModel } from '../Shared/Models/Generic';
import { LoginModel, PropsType } from './Model';
import { LoadingBar } from '../index';
import api from '../../api/Connect';

const Home = ({ setAuthenticated }: PropsType) => {
    const [isLoading, setLoading] = useState<boolean>(false);
    const [loginDetails, setLoginDetails] = useState<LoginModel>({
        user: '',
        password: ''
    });

    const handleChange = (e: OnChangeEventModel) => {
        setLoginDetails({
            ...loginDetails,
            [e.target.name]: e.target.value
        });
    };

    const onFormSubmit = async (e: FormEvent) => {
        e.preventDefault();
        try {
            setLoading(true);
            const {user, password} = loginDetails;
            const response = await api.post('/auth/login', { user, password });
            const token = response.data.token;
            if (token) {
                localStorage.setItem('token', token);
                setAuthenticated(token);
            }
        } catch(error) {
            setLoading(false);
        }
    };

    return (
        <div className="login-container">
            <div className="login-box animated fadeInDown">

                <img src="/logo.svg" alt="" />

                <LoadingBar isLoading={isLoading} />

                <form onSubmit={onFormSubmit}>
                    <div className="input-group mb-3">
                        <div className="input-group-prepend">
                            <span className="input-group-text"><i className="fas fa-user"></i></span>
                        </div>
                        <input type="text" name="user" value={loginDetails.user} onChange={handleChange} className="form-control" placeholder="Usuário" aria-label="Usuário" disabled={isLoading} />
                    </div>

                    <div className="input-group mb-3">
                        <div className="input-group-prepend">
                            <span className="input-group-text"><i className="fas fa-key"></i></span>
                        </div>
                        <input type="password" name="password" value={loginDetails.password} onChange={handleChange} className="form-control" placeholder="Senha" aria-label="Senha" disabled={isLoading} />
                    </div>

                    <input className="btn btn-primary btn-fill w-100" type="submit" value="LOGIN" disabled={isLoading} />
                </form>

            </div>
        </div>
    );
};

export default Home;