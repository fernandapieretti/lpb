import React from 'react';
import { AdminHome } from '../index';
import { PropsListingModel } from '../Shared/Admin/Listing/Model';
import { TableHeaderModel } from '../Shared/Table/Model';

const Home = ({ term }: PropsListingModel) => {
    const title: string = 'Clientes';
    
    const directory: string = 'clientes';
    
    const header: TableHeaderModel[] = [
        { headerName: 'Clientes', field: 'clientes' },
        { headerName: 'Ações', field: 'acoes' }
    ];

    return (
        <AdminHome title={title} directory={directory} header={header} term={term} />
    );
};

export default Home;