import React, { useEffect, useState } from 'react';
import { OnChangeEventModel } from '../Shared/Models/Generic';
import { ClienteModel } from './Model';
import { PropsManageModel } from '../Shared/Admin/Manage/Model';

const Manage = ({ handleSubmit, data, isLoading, button }: PropsManageModel<ClienteModel>) => {
    const [cliente, setCliente] = useState<ClienteModel>({
        id: null,
        titulo: ''
    });

    const handleChange = (event: OnChangeEventModel) => {
        setCliente({
            ...cliente,
            [event.target.name]: event.target.value
        });
    };

    useEffect(() => {
        if (data.id) {
            setCliente(data);
        }
    }, [data]);

    return (
        <form onSubmit={e => handleSubmit(e, cliente)}>

            <div className="card">
                <div className="content">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="form-group">
                                <label>Titulo *</label>
                                <input type="text" className="form-control" name="titulo" value={cliente.titulo} onChange={handleChange} disabled={isLoading} required />
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div className="button-wrapper d-flex justify-content-end">
                { button }
                <button type="submit" className="btn btn-primary btn-fill ml-4" disabled={isLoading}>SALVAR</button>
            </div>
        </form>
    );
};

export default Manage;