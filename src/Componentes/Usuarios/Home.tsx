import React from 'react';
import { AdminHome } from '../index';
import { PropsListingModel } from '../Shared/Admin/Listing/Model';
import { TableHeaderModel } from '../Shared/Table/Model';

const Home = ({ term }: PropsListingModel) => {
    const title: string = 'Usuarios';
    
    const directory: string = 'usuarios';
    
    const header: TableHeaderModel[] = [
        { headerName: 'Usuarios', field: 'usuarios' },
        { headerName: 'Ações', field: 'acoes' }
    ];

    return (
        <AdminHome title={title} directory={directory} header={header} term={term} />
    );
};

export default Home;