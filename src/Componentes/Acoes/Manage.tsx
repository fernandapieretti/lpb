import React, { useEffect, useState } from 'react';
import { UploadMultiFiles } from '..';
import { OnChangeEventModel } from '../Shared/Models/Generic';
import { AcoesModel } from './Model';
import { PropsManageModel } from '../Shared/Admin/Manage/Model';
const CKEditor = require('ckeditor4-react');

const Manage = ({ handleSubmit, data, isLoading, button }: PropsManageModel<AcoesModel>) => {
    let formData = new FormData();

    const [acoes, setAcoes] = useState<AcoesModel>({
        id: null,
        titulo: '',
        conteudo: '',
        files: [],
        files2: []
    });

    const [conteudo, setConteudo] = useState<string>('');

    const handleChange = (event: OnChangeEventModel) => {
        setAcoes({
            ...acoes,
            [event.target.name]: event.target.value
        });
    };

    const handleEditorChange = (e: any) => {
        setConteudo(e.editor.getData());
        const event = { target: { name: 'conteudo', value: conteudo } };
        handleChange(event);
    };
    
    useEffect(() => {
        if (data.id) {
            setAcoes(data);
        }
    }, [data]);

    useEffect(() => {
        formData = new FormData();
        formData.append('_method', 'PUT');
        formData.append('titulo', acoes.titulo);
        formData.append('conteudo', conteudo);

        if (data.id) { formData.append('id', data.id.toString()); }


        for (let i = 0; i < acoes.files.length; i++) {
            formData.append("files[]", acoes.files[i], acoes.files[i].name);
        }

        for (let i = 0; i < acoes.files2.length; i++) {
            formData.append("files2[]", acoes.files2[i], acoes.files2[i].name);
        }
        
    }, [acoes]);

    return (
        <form onSubmit={e => handleSubmit(e, formData)}>

            <div className="card">
                <div className="content">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="form-group">
                                <label>Titulo *</label>
                                <input type="text" className="form-control" name="titulo" value={acoes.titulo} onChange={handleChange} disabled={isLoading} required />
                            </div>
                        </div>
                        <div className="col-md-12">
                            <div className="form-group">
                                <label>Conteudo *</label>
                                <CKEditor data={conteudo} onChange={handleEditorChange} readOnly={isLoading} />
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Anexos 1 *</label>
                                <UploadMultiFiles field={'files'} onSelectDocuments={handleChange} isLoading={isLoading} acceptedExtensions={'png|jpg|jpeg|gif'} />
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label>Anexos 2 *</label>
                                <UploadMultiFiles field={'files2'} onSelectDocuments={handleChange} isLoading={isLoading} acceptedExtensions={'png|jpg|jpeg|gif'} />
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div className="button-wrapper d-flex justify-content-end">
                {button}
                <button type="submit" className="btn btn-primary btn-fill ml-4" disabled={isLoading}>SALVAR</button>
            </div>
        </form>
    );
};

export default Manage;