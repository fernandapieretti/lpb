import React from 'react';
import { AdminHome } from '../index';
import { TableHeaderModel } from '../Shared/Table/Model';
import { PropsListingModel } from '../Shared/Admin/Listing/Model';

const Home = ({ term }: PropsListingModel) => {
    const title: string = 'Acoes';
    
    const directory: string = 'acoes';
    
    const header: TableHeaderModel[] = [
        { headerName: 'Acao', field: 'acao' },
        { headerName: 'Ações', field: 'acoes' }
    ];

    return (
        <AdminHome title={title} directory={directory} header={header} term={term} />
    );
};

export default Home;