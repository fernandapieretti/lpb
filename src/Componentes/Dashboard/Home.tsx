import React from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import { AdminLayout } from '..';

const Home = () => {
    const title: string = 'Dashboard';

    const lineOptions: Highcharts.Options = {
        title: {
            text: 'Line chart'
        },
        series: [{
            type: 'line',
            data: [1, 2, 3]
        }]
    };

    const barOptions: Highcharts.Options = {
        title: {
            text: 'Bar chart'
        },
        series: [{
            type: 'bar',
            data: [1, 2, 3]
        }]
    };

    const areaOptions: Highcharts.Options = {
        title: {
            text: 'Area chart'
        },
        series: [{
            type: 'area',
            data: [1, 2, 3]
        }]
    };

    const columnOptions: Highcharts.Options = {
        title: {
            text: 'Column chart'
        },
        series: [{
            type: 'column',
            data: [1, 2, 3]
        }]
    };

    return (
        <AdminLayout title={title} button={null}>
            <div className="row">

                <div className="col-md-6">
                    <div className="card">
                        <div className="content">
                            <HighchartsReact
                                highcharts={Highcharts}
                                options={lineOptions}
                            />
                        </div>
                    </div>
                </div>

                <div className="col-md-6">
                    <div className="card">
                        <div className="content">
                            <HighchartsReact
                                highcharts={Highcharts}
                                options={barOptions}
                            />
                        </div>
                    </div>
                </div>

                <div className="col-md-6">
                    <div className="card">
                        <div className="content">
                            <HighchartsReact
                                highcharts={Highcharts}
                                options={columnOptions}
                            />
                        </div>
                    </div>
                </div>

                <div className="col-md-6">
                    <div className="card">
                        <div className="content">
                            <HighchartsReact
                                highcharts={Highcharts}
                                options={areaOptions}
                            />
                        </div>
                    </div>
                </div>

            </div>
        </AdminLayout>
    );
};

export default Home;