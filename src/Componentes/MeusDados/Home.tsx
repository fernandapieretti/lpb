import React from 'react';
import { AdminHome } from '../index';
import { PropsListingModel } from '../Shared/Admin/Listing/Model';
import { TableHeaderModel } from '../Shared/Table/Model';

const Home = ({ term }: PropsListingModel) => {
    const title: string = 'Meus Dados';
    
    const directory: string = 'meus-dados';
    
    const header: TableHeaderModel[] = [
        { headerName: 'Meus Dados', field: 'dados' },
        { headerName: 'Ações', field: 'acoes' }
    ];

    return (
        <AdminHome title={title} directory={directory} header={header} term={term} />
    );
};

export default Home;