/* Components */
export { default as Dashboard } from './Dashboard/Home';
export { default as Login } from './Login/Home';
export { default as Acoes } from './Acoes/Home';
export { default as ManageAcoes } from './Acoes/Manage';
export { default as Clientes } from './Clientes/Home';
export { default as ManageClientes } from './Clientes/Manage';
export { default as ModelosAcao } from './ModelosAcao/Home';
export { default as ManageModelosAcao } from './ModelosAcao/Manage';
export { default as Minutas } from './Minutas/Home';
export { default as ManageMinutas } from './Minutas/Manage';
export { default as Usuarios } from './Usuarios/Home';
export { default as ManageUsuarios } from './Usuarios/Manage';
export { default as MeusDados } from './MeusDados/Home';
export { default as ManageMeusDados } from './MeusDados/Manage';

/* Shared */
export { default as LoadingBar } from './Shared/LoadingBar/LoadingBar';
export { default as Button } from './Shared/Button/Button';
export { default as Table } from './Shared/Table/Table';
export { default as Pagination } from './Shared/Pagination/Pagination';
export { default as UploadMultiFiles } from './Shared/UploadMultiFiles/UploadMultiFiles';

/* Admin Shared */
export { default as AdminHome } from './Shared/Admin/Home/Home';
export { default as AdminLayout } from './Shared/Admin/Layout/Layout';
export { default as AdminListing } from './Shared/Admin/Listing/Listing';
export { default as AdminManage } from './Shared/Admin/Manage/Manage';

/* Template Structure */
export { default as Layout } from './Structure/Layout';
export { default as Header } from './Structure/Header';
export { default as Nav } from './Structure/Nav';
export { default as Search } from './Structure/Search';
