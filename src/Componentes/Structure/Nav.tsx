import React from 'react';
import { Link } from 'react-router-dom';
import { NavLinksModel } from '../Shared/Models/Generic';

const Nav = () => {

    const links: NavLinksModel[] = [
        {
            link: '/admin/dashboard',
            icone: 'fas fa-chart-pie',
            name: 'Dashboard'
        },
        {
            link: '/admin/acoes',
            icone: 'fas fa-layer-group',
            name: 'Acoes'
        },
        {
            link: '/admin/clientes',
            icone: 'far fa-user',
            name: 'Clientes'
        },
        {
            link: '/admin/minutas',
            icone: 'fas fa-scroll',
            name: 'Minutas'
        },
        {
            link: '/admin/modelos-de-acao',
            icone: 'far fa-file-alt',
            name: 'Modelos de Acao'
        },
        {
            link: '/admin/usuarios',
            icone: 'fas fa-id-card',
            name: 'Usuarios'
        }
    ];
    
    return (
        <div className="sidebar">
            <div className="sidebar-wrapper">
                <div className="logo">
                    <img src="/logo-white.svg" alt="" />
                </div>
                <ul className="nav">
                    { links.map((item: NavLinksModel) => {
                        return (
                            <li key={item.link}>
                                <Link to={item.link}>
                                    <i className={item.icone}></i>
                                    <p>{ item.name }</p>
                                </Link>
                            </li>
                        )
                    }) }
                </ul>
            </div>
        </div>
    );
};

export default Nav;