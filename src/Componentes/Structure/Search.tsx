import React from 'react';

const Search = ({ term, onChangeTerm}: any) => {
    return (
        <form className="form">
            <input type="text" placeholder="Pesquise aqui..." value={term} onChange={(e) => onChangeTerm(e.target.value)} />
        </form>
    );
};

export default Search;