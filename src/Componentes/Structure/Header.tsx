import React, { useState } from 'react';
import { Link } from "react-router-dom";
import { toast } from 'react-toastify';
import { Search, LoadingBar } from '../index';
import api from '../../api/Connect';

const Header = ({ term, onChangeTerm, setAuthenticated, toggleMenu }: any) => {
    const [isLoading, setLoading] = useState<boolean>(false);

    const logOut = async () => {
        setLoading(true);
        try {
            await api.post('/auth/logout');
            localStorage.removeItem('token');
            setAuthenticated(null);
            toast.success('Ate logo!');
        } catch (error) {
            setLoading(false);
        }
    };

    return (
        <React.Fragment>
            <nav className="navbar navbar-expand-lg navbar-light">
                <div className="d-none d-md-block">
                    <Search
                        term={term}
                        onChangeTerm={onChangeTerm}
                    />
                </div>

                <ul className="navbar-nav my-2 my-lg-0 ml-auto">
                    <li className="nav-item">
                        <a href="#" className='nav-link'>
                            OLA, USUARIO
                        </a>
                    </li>
                    <li className="nav-item">
                        <Link to='/admin/meus-dados' className='nav-link'>
                            <i className='fas fa-user-shield'></i>
                            <span className="d-none d-md-inline ml-2">MEUS DADOS</span>
                        </Link>
                    </li>
                    <li className="nav-item">
                        <a href="#" className="nav-link" onClick={() => logOut()}>
                            <i className="fas fa-sign-out-alt"></i>
                            <span className="d-none d-md-inline ml-2">SAIR</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a href="#" className="nav-link navbar-toggler" onClick={() => toggleMenu()}>
                            <i className="fas fa-ellipsis-v"></i>
                        </a>
                    </li>
                </ul>

            </nav>

            <LoadingBar isLoading={isLoading} />
        </React.Fragment>
    );
};

export default Header;