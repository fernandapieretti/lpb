import React, { useState, useEffect } from 'react';
import { Redirect, Switch, Route } from 'react-router-dom';
import { Header, Nav, MeusDados, Dashboard, Acoes, Clientes, ModelosAcao, Minutas, Usuarios } from '../index';
import 'react-confirm-alert/src/react-confirm-alert.css';

const Layout = ({ setAuthenticated }: any) => {
    const [isMenuOpened, toggleMenu] = useState<boolean>(false);
    const [q, setQ] = useState<string>('');
    const [term, setTerm] = useState<string>('');

    useEffect(() => {
        const debounce = setTimeout(() => {
            setTerm(q);
        }, 800);
        return () => {
            clearInterval(debounce);
        };
    }, [q]);

    return (
        <div className={isMenuOpened ? 'wrapper nav-open' : 'wrapper'}>
            <Nav />
            <div className="main-panel">
                <Header
                    toggleMenu={() => toggleMenu(!isMenuOpened)}
                    term={q}
                    onChangeTerm={setQ}
                    setAuthenticated={setAuthenticated}
                />
                <div className="content">
                    <div className="container-fluid">
                        <Switch>
                            <Route exact path='/admin' render={() => (<Redirect to="/admin/dashboard" />)} />
                            <Route path='/admin/dashboard'><Dashboard /></Route>
                            <Route path='/admin/clientes'><Clientes term={term} /></Route>
                            <Route path='/admin/acoes'><Acoes term={term} /></Route>
                            <Route path='/admin/meus-dados'><MeusDados term={term} /></Route>
                            <Route path='/admin/modelos-de-acao'><ModelosAcao term={term} /></Route>
                            <Route path='/admin/minutas'><Minutas term={term} /></Route>
                            <Route path='/admin/usuarios'><Usuarios term={term} /></Route>
                        </Switch>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Layout;