import React from 'react';
import { AdminHome } from '../index';
import { PropsListingModel } from '../Shared/Admin/Listing/Model';
import { TableHeaderModel } from '../Shared/Table/Model';

const Home = ({ term }: PropsListingModel) => {
    const title: string = 'Modelos de Acao';
    
    const directory: string = 'modelos-de-acao';
    
    const header: TableHeaderModel[] = [
        { headerName: 'Modelos de Acao', field: 'modelos-de-acao' },
        { headerName: 'Ações', field: 'acoes' }
    ];

    return (
        <AdminHome title={title} directory={directory} header={header} term={term} />
    );
};

export default Home;